package tp3;

public class TestCfg {
	
	public static void testSimpleIf() {
		if(true) {
			System.out.println("");
		} else {
			int b = 0;
		}
	}

	public static void testElseIfElse() {
		if(true) {
			System.out.println("");
		} else if(false) {
			int b = 0;
		} else {
			;
		}
	}

	public static void testElseIf() {
		if(true) {
			System.out.println("");
		} else if(false) {
			int b = 0;
		}
	}


	public static void testDoWhile() {
		do {
			System.out.println("");
		} while(true);
	}

	public static void testDoWhileBreak() {
		do {
			System.out.println("");
			if(true){
				break;
			}
		} while(true);
	}

	public static void testDoWhileContinue() {
		do {
			System.out.println("");
			if(true){
				continue;
			}
		} while(true);
	}

	public void testWhileBreaks() {
		int i = 0; 
		int j = 5;
		while(i<10) {
			i++;
			if (i==j) {
				break;
			} else {
				;
			}
		}
	}

	public void testWhileContinue() {
		int i = 0; 
		int j = 5;
		while(i<10) {
			i++;
			if (i==j) {
				continue;
			} else {
				;
			}
		}
	}
	
	public void testWhileBreakContinue() {
		int i = 0; 
		int j = 5;
		while(i<10) {
			i++;
			if (i==j) {
				break;
			} else {
				continue;
			}
		}
	}  


	public void testDoubleWhile() {
		int i = 0; 
		int j = 5;
		while(i<10) {
			i++;
			while (j<i) {
				System.out.println("hey");
			}
		}
	}

	public void testDoubleWhileBreak() {
		int i = 0; 
		int j = 5;
		while(i<10) {
			i++;
			while (j<i) {
				if (i==j) {
					break;
				}
			}
		}
	}

	public void testDoubleWhileLabeledBreak() {
		int i = 0; 
		int j = 5;

		go:
			while(i<10) {
				i++;
				while (j<i) {
					if (i==j) {
						break go;
					}
				}
			}
	}

	public void testDoubleForContinue(){
		for(int i = 0; i<10; i++) {
			System.out.println(i);
			for(int j = 0; j<10; j++) {
				System.out.println(j);
				if(j==8) {
					continue;
				}
			}
		}
	}

	public void testDoubleForLabeledContinue(){
		ici:
			for(int i = 0; i<10; i++) {
				System.out.println(i);
				for(int j = 0; j<10; j++) {
					System.out.println(j);
					if(j==8) {
						continue ici;
					}
				}
			}
	}

	

	private void testSwitch(int k){
		switch(k){
			case 0: 
				int i = 0;
				break;
			case 3: 
				int j = 0;
			default: k=-1;
				break;
		}
	} 

/*
	public void testWhileContinueInstructionAfter() {
		int i = 0; 
		int j = 5;
		while(i<10) {
			i++;
			if (i==j) {
				break;
			} else {
				continue;
				int c;
			}
		}
	}
	
	public void testDoubleForLabelledContinueButNoLabel(){
		for(int i = 0; i<10; i++) {
			System.out.println(i);
			for(int j = 0; j<10; j++) {
				System.out.println(j);
				if(j==8) {
					continue ici;
				}
			}
		}
	} 

	public void testInstructionAfterReturn() {
		if(true) {
			System.out.println("");
			return;
			int c;
		} else {
			int b = 0;
		}
	}

	*/
}
