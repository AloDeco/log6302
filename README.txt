TP1
Visiteur : MetricsVisitor.java
Commande : make tp1
Sortie : fichier json.txt dans la racine du projet


TP2
Visiteur : UmlVisitor.java 
Dossier associé : uml/
Commande : make tp2
Sortie : diagrammes de classe uml dans uml/graphs/

TP2 - pour récupérer le nom des classes du projet
Visiteur : ClassVisitor.java
Commande : make classnames
Sortie : fichier classNamesList.txt dans le dossier uml/


TP3
Visiteur : CfgVisitor.java 
Dossiers associés : 
- tp3_methods/ pour les méthodes à analyser. Executer le script listFiles.sh tp3_methods du dossier racine pour mettre à jour la liste des fichiers du dossier tp3_methods/ si nécessaire.  fichiers du dossier tp3_methods/ si nécessaire. 
- cfg/
Commande : make tp3
Sortie : le cfg dans le dossier cfg/graphs/

TP4
Visiteur : CfgVisitor.java 
Dossiers associés : 
- tp3_methods/ pour les méthodes à analyser. Executer le script listFiles.sh tp3_methods du dossier racine pour mettre à jour la liste des fichiers du dossier tp3_methods/ si nécessaire. 
- cfg/
Commande : make tp4
Sortie : le cfg, l'arbre des dominateurs, l'arbre des postdominateurs et le CFG avec les définitions valides dans le dossier cfg/graphs/


TP5
Visiteur : CfgVisitor.java 
Dossiers associés : 
- tp3_methods/ pour les méthodes à analyser. Executer le script listFiles.sh tp3_methods du dossier racine pour mettre à jour la liste des fichiers du dossier tp3_methods/ si nécessaire. 
- cfg/
Commande : make tp5
Sortie : le cfg, l'arbre des dominateurs, l'arbre des postdominateurs et le CFG avec les définitions valides, les dépendances de contrôle, les dépendances de données, les dépendances de programme et le slicing dans le dossier cfg/graphs/