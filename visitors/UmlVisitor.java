package javaparser;


import java.io.FileWriter;
import java.io.IOException;
import java.util.*; 
import java.util.regex.Pattern;

public class UmlVisitor extends AbstractVisitor
{
	private String pack = ""; 
	private String currentClassName; 
	Set<String> classNamesAll = new HashSet<String>();   //not useful to keep two classes with same name as it won't work with GraphViz
	ArrayList<String> classNamesForMethods = new ArrayList<String>();
  	ArrayList<String> methodNames = new ArrayList<String>();
  	ArrayList<String> methodArgs = new ArrayList<String>();
  	ArrayList<String> methodtype = new ArrayList<String>();
  	ArrayList<String> classNamesForFields = new ArrayList<String>();
  	ArrayList<String> fields = new ArrayList<String>();

  	Set<String> inheritance = new HashSet<String>();
  	Set<String> composition = new HashSet<String>();

  	Set<String> standardtypes;

  	public UmlVisitor(JavaTypes javatypes) {
		this.standardtypes = javatypes.classes;
  	}

  	public Object visit(CompilationUnit node, Object data){
  		Token token = node.jjtGetFirstToken();
  		String packname = "";
  		if(token.image.equals("package")) {
  			token = token.next;
  			while (token.kind != 102) {
  				packname += token.image;
  				token = token.next;
  			}
  		}
  		for (int i = 0; i<packname.length(); i++) {
  			if (packname.charAt(i)!= '.') {
  				pack += packname.charAt(i);  //remove all the dots because files will be named after it and it would cause troubles
  			}
  		}

  		if(pack.equals("")) {
  			pack = "NoPackages";
  		}
		propagate(node, data);
		return data;
	}

	public Object visit(ClassDeclaration node, Object data){
		
		Token token = node.jjtGetFirstToken();
		while (!token.image.equals("class")) { 
			token = token.next;
		}
		Token className = token.next;
		if (className.kind == 97 || className.kind == 102 || className.kind == 103 || className.kind == 104) { //if it is ) or ; or , or .
			return data;
		}
		
		currentClassName = verifyClassName(className.image); 
		classNamesAll.add(currentClassName);


		if (className.next.kind==64) {  //<
			int nbOpen = 1; //number of < 
			int nbClose = 0; //number of >
			className = className.next.next;

			while(nbOpen != nbClose) {
				if (className.kind==64) {
					nbOpen++;
				} else if (className.kind==63) {
					nbClose++;
				}
				className = className.next;
			}
		} else {
			className = className.next;
		} 

		if (className.image.equals("extends")) {
			String superClassName = verifyClassName(className.next.image);
			inheritance.add(currentClassName + "->" + superClassName);  //add inheritance relation
		}
		
		propagate(node, data);
		return data;
	}

	private String verifyClassName(String name) {
		//the name of a class can't be Node or Edge because it causes a problem with GraphViz, so Class is appended
		if (name.equals("Node")) {
			return "NodeClass";
		} else if (name.equals("Edge")) {
			return "EdgeClass";
		}
		return name;
	}

	public Object visit(VoidMethodDecl node, Object data){
  		String type = "";
		Token token = node.jjtGetFirstToken();
		while (!token.image.equals("void")) {
			type = type + token.image + " ";
			token = token.next;
		}
		type = type + token.image;
		methodtype.add(type);

		getMethodInfo(token.next);	//token.next contains the name of the method	
		propagate(node, data);
		return data;
	}
 
 	public Object visit(MethodOrFieldDecl node, Object data){
		Token token = node.jjtGetFirstToken();
		boolean isMethod = isMethod(token);
		if (isMethod) {  //if it is a method
			methodCase(token);
		} else {  //if it is not a method, then it is a field
			fieldCase(token);
		}
		propagate(node, data);
		return data;
	}

	private boolean isMethod(Token token) {// determines if it is a method in public Object visit(MethodOrFieldDecl node, Object data)
		int beginLineFirstToken = token.beginLine; 
		while (token != null && token.kind!=61) { // 61 is equal
			if (token.kind == 96 && token.beginLine == beginLineFirstToken) {  // 96 is links parenthesis (
				return true;
			}
			token = token.next;
		}
		return false; 
	}

	private void methodCase(Token token) { // if it is a method in public Object visit(MethodOrFieldDecl node, Object data)	
		Boolean dot = false;
		String type = "";
		/* if type is composed like A.B */
		if (token.next.kind == 104) {  //it is a dot
			dot = true;
			type += token.image;
			while (token.next != null && (token.kind == 104 || token.next.kind == 104)) {
				if (token.next.kind == 104) {
					type += token.next.image;
				} else if (token.kind == 104) {
					type += token.next.image;
				} 
				token = token.next;
			}
		}
		/* if type is a list */
		if (token.next.kind == 64) { //next token is an opening <
			type = type + token.image + "\\" + token.next.image;
			token = token.next.next; //symbol after the first <
			int nbOpen = 1; //number of < 
			int nbClose = 0; //number of >
			while(nbOpen != nbClose) {
				if (token.kind==64) {
					type += "\\" + token.image;  //escape <
					nbOpen++;
				} else if (token.kind==63) {
					type += "\\" + token.image;   //escape >
					nbClose++;
				} else {
					type += token.image;
				}
				token = token.next;
			}
		} else if (token.next.kind == 100) { //next token is an opening [
			type = type + token.image + "\\" + token.next.image;
			token = token.next.next; //symbol after the first [
			int nbOpen = 1; //number of [
			int nbClose = 0; //number of ]
			while(nbOpen != nbClose) {
				if (token.kind==100) {
					type += "\\" + token.image;  //escape [
					nbOpen++;
				} else if (token.kind==101) {
					type += "\\" + token.image;   //escape ]
					nbClose++;
				} else {
					type += token.image;
				}
				token = token.next;
			}
		} else if(!dot) {
			type = token.image; 
			token = token.next; //after the type there is the name of the method in token.next except if it is a dot	
		}
		methodtype.add(type); 
		getMethodInfo(token); //name of the method in token
	}

	public void getMethodInfo(Token methodName){
		methodNames.add(methodName.image);
		classNamesForMethods.add(currentClassName);
		int line = methodName.beginLine;
		String args = "";

		Token token = methodName.next.next;  //methodName.next is the links parenthesis
		if (token.kind != 97) { //it is not a right parenthesis
			while (token.kind != 97) {
				if (token.kind == 105) {  //105 is @
					token = token.next;
				}
				if (token.kind == 64 || token.kind == 100 || token.next.kind == 63 || token.next.kind == 64 || token.next.kind == 100 || token.next.kind == 101) { //64 is <, 63 is >, 100 is [, 101 is ]
					args = args + token.image; //don't write spaces
				} else {
					args = args + token.image + " ";
				}
				token = token.next;
			}
		}
		String argsNormalized = argNormalizer(args);
		methodArgs.add(argsNormalized);
	}

	private String argNormalizer(String args) { // to normalise arguments of a method for graphViz
		String[] s = args.split(","); 
		String res = "";
		for (int i = 0; i< s.length; i++) {
			res = res + s[i].trim().split("\\s+")[0] + ", ";
		}
		res = res.substring(0, res.length()-2); //remove last ","

		String resEscaped = "";
		for (int i = 0; i<res.length(); i++) {
			if (res.charAt(i) == '<'){
				resEscaped += "\\<";
			} else if (res.charAt(i) == '>'){
				resEscaped += "\\>";
			} else {
				resEscaped += res.charAt(i);
			}
		}

		return resEscaped;
	}

	private void fieldCase(Token token) {  // if it is a field in public Object visit(MethodOrFieldDecl node, Object data)	
		classNamesForFields.add(currentClassName);
		if (token.next.kind == 64 || token.next.kind == 100) { //if the next token is < or [ then the type is a list
			fields.add(typeList(token, ""));
			//composition is called in typeList()
		} else if (token.next.kind == 104) {  // if the next token is a point .
			fields.add(typeComposed(token));
			//composition is called in typeComposed()
		} else {
			fields.add(token.next.image + " : " + token.image);  //token.next.image is the name of the field and token.image is its type
			isComposition(token.image);
		}
	}

	private String typeList(Token token, String type) {   //if token.next.kind==64 or 100 (< or [)
		Token opening = token.next;  // contains the token for < or [
		if(type.equals("")) { // called from fieldCase
			type = type + token.image + "\\" + token.next.image; 
		} else {  // called from typeComposed
			type = type + "\\" + token.next.image; 
		}
		
		token = token.next.next;  //token after opening < or [
		int nbOpen = 1; //number of < or [
		int nbClose = 0; //number of > or ]
		if (opening.kind == 64) {
			while(nbOpen != nbClose) {
				if (token.kind==64) {
					type += "\\" + token.image;  //escape <
					nbOpen++;
				} else if (token.kind==63) {
					type += "\\" + token.image;   //escape >
					nbClose++;
				} else if (token.kind==100) {
					type += "\\" + token.image;   //escape [   -> there can be a [] list inside a <>
				} else if (token.kind==101) {
					type += "\\" + token.image;   //escape ]
				} else {
					type += token.image;
				}
				token = token.next;
			}
		} else if (opening.kind == 100) { //next token is an opening [
			while(nbOpen != nbClose) {
				if (token.kind==100) {
					type += "\\" + token.image;  //escape [
					nbOpen++;
				} else if (token.kind==101) {
					type += "\\" + token.image;   //escape ]
					nbClose++;
				} else if (token.kind==64) {
					type += "\\" + token.image;  //escape <
				} else if (token.kind==63) {
					type += "\\" + token.image;   //escape >
				} else {
					type += token.image;
				}
				token = token.next;
			}
		}
		isComposition(type);
		type = token.image + " : " + type; //token.image is the name of the variable
		return type;
	}

	private String typeComposed(Token token) {   //if token.next.kind==104 (.)
		String type = token.image; 
		
		while (token.next != null && (token.kind == 104 || token.next.kind == 104)) {
			if (token.next.kind == 104) {
				//type = type + "\\" + token.next.image;
				type += token.next.image;
			} else if (token.kind == 104) {
				type += token.next.image;
			} 
			token = token.next;
		}
		if(token.next.kind == 64 || token.next.kind == 100) { // 64 is < and 100 is [ so it is a list
			return typeList(token, type);
		} 
		isComposition(type);
		type = token.next.image + " : " + type;
		return type;
	}

	public void addInheritance(String filename) { //to write inheritance relation in text file
		if(inheritance.isEmpty()) { return; }
		try {
	        FileWriter file = new FileWriter(filename, true);
	        for(String rel : inheritance) {
	        	file.write(rel + "\n");
	        }
	        
	        file.flush();
      	} catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void addComposition(String filename) { //to write composition relation in text file
		if(composition.isEmpty()) { return; }
		try {
	        FileWriter file = new FileWriter(filename, true);
	        for(String rel : composition) {
	        	file.write(rel + "\n");
	        }
	        
	        file.flush();
      	} catch (IOException e) {
            e.printStackTrace();
        }
	}

	private void isComposition(String type) {   //to test if a type is a composition ie the type is a class from another file
		String[] s = type.split(",");
		for (int i = 0; i<s.length ; i++) {
			type = verifyTypeComposition(s[i]);
			type = verifyClassName(type);  //if the type is Node or Edge
			//if (standardtypes.contains(type) || type.equals("")) {return;}
			if (!standardtypes.contains(type) || type.equals("") || type.equals(currentClassName)) {return;}
			try {
		        FileWriter file = new FileWriter("./uml/composition/" + pack + ".txt", true);
		        file.write(type + "->" + currentClassName+ "\n");
		        file.flush();
	      	} catch (IOException e) {
	            e.printStackTrace();
	        }
		}
	}

	private String verifyTypeComposition(String type) {
		/* get only the string corresponding to the type, not Arraylist or String[] */
		String[] s;
		// remove . and what is after
		s = type.split("\\.");
		type = s[0];

		// remove /< and /> and what is not between them
		s = type.split("\\<");
		type = s[s.length -1];
		s = type.split("\\\\>");
		type = s[0];

		// remove /[ and /] and what is between them
		s = type.split("\\\\\\[");
		type = s[0];

		if (type.contains("?")) {  // private final Coder<? super Object> coder;  -> type = ?superObject : we don't want it
			return "";
		}

		return type;
	}

	public String getPackage() {
		return pack;
	} 

    public void Results(UmlWriter writer) {
    	
    	try {
    		writer.writeFile(classNamesAll, classNamesForMethods, methodNames, methodtype, methodArgs, classNamesForFields, fields);
    	}catch (IOException e) {
            e.printStackTrace();
        }
    	
    }

}
