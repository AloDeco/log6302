package javaparser;


import java.io.FileWriter;
import java.io.IOException;
import java.util.*; 
import java.util.Dictionary;

public class CfgVisitor extends AbstractVisitor
{
	private int fileNumber;
	private String methodName;
	private int methodId;
	private int TP_NUMBER;
	private CfgGraph graph = null;
	private int id;
	private int currentPreviousId;
	private CfgNode currentNode;
	private HashMap<Integer, ArrayList<Integer>> idInfo = new HashMap<Integer, ArrayList<Integer>>();
	private HashMap<String, ArrayList<Integer>> identifiers = new HashMap<String, ArrayList<Integer>>(); 
	private HashMap<Integer, Integer> switchDefaultId = new HashMap<Integer, Integer>(); 

	private HashMap<CfgNode, ArrayList<CfgNode>> postdominatorsMap; // for TP5
	private HashMap<CfgNode, CfgNode> directPostDoms; // for TP5
	private HashMap<CfgNode, ArrayList<CfgNode>> inReachingDef;  // for TP5

	/* Execution time mesurement */
	private long startTime;
	private long endTime;
   

	public CfgVisitor(int fileNumber, int TP_NUMBER) {
		this.fileNumber = fileNumber;
		this.TP_NUMBER = TP_NUMBER; 
		this.id = -1;
		this.methodId = 0;
		this.currentPreviousId = -1;
		this.startTime = System.nanoTime();
	}

	private int getNewId() {
		this.id++;
		return this.id;
	}
	private int getId() {
		return this.id;
	}
	private int getPreviousId() {
		return this.currentPreviousId;
	}
	private void setPreviousId(int id) {
		this.currentPreviousId = id;
	}

	private ArrayList<Integer> getIdInfo(int idNode) {
		return this.idInfo.remove(idNode);
	}
	private void setIdInfo(int idNode, int idLast) {
		ArrayList<Integer> list;
		if (this.idInfo.containsKey(idNode)) {
			list = this.idInfo.remove(idNode);
		} else {
			list = new ArrayList<Integer>();
		}
		list.add(idLast);
		this.idInfo.put(idNode, list);
	}
	private boolean containsKey(int idNode) {
		return this.idInfo.containsKey(idNode);
	}
	private void addIdentifier(String label, int beginId, int endId) {
		ArrayList<Integer> info = new ArrayList<Integer>();
		info.add(beginId);
		info.add(endId);
		this.identifiers.put(label, info);
	}
	private ArrayList<Integer> getIdentifier(String label) {
		return this.identifiers.remove(label);
	}
	private boolean identifiersContains(String label) {
		return this.identifiers.containsKey(label);
	}
	private void setSwitchDefaultId(int beginId, int defaultId) {
		this.switchDefaultId.put(beginId, defaultId);
	}
	private int getSwitchDefaultId(int beginId){
		return this.switchDefaultId.remove(beginId);
	}


	@Override
	protected void propagate(Node node, Object data){
        for(int i = 0; i < node.jjtGetNumChildren(); ++i){
            node.jjtGetChild(i).jjtAccept(this, data);
        }
    }


    /******* Methods visits *******/
	@Override
	public Object visit(VoidMethodDecl node, Object data){
		this.methodName = ((Identifier)node.jjtGetChild(0)).jjtGetFirstToken().image;
		return visitMethod(node, data);
	}
	@Override
	public Object visit(MethodOrFieldDecl node, Object data){
		if(!isMethod(node.jjtGetFirstToken())) {
			propagate(node, data);
			return data;
		}
		this.methodName = ((Identifier)node.jjtGetChild(1)).jjtGetFirstToken().image;
		return visitMethod(node, data);
	}
	private boolean isMethod(Token token) {// determines if it is a method in public Object visit(MethodOrFieldDecl node, Object data)
		int beginLineFirstToken = token.beginLine; 
		while (token != null && token.kind!=61) { // 61 is equal
			if (token.kind == 96 && token.beginLine == beginLineFirstToken) {  // 96 is links parenthesis (
				return true;
			}
			token = token.next;
		}
		return false; 
	}

	private Object visitMethod(SimpleNode node, Object data) {
		graph = new CfgGraph(this.methodName);	
		
		createAndRegisterNode("Entry", null);
		
		graph.setCurrentLastid(getId());

		propagate(node, data);

		graph.createLastRelation();
		graph.verifyGraph();

		writeResults();

		reset();
		return data;
	}


	/******* Conditional Instructions Statements *******/
	@Override
	public Object visit(IfStatement node, Object data){
		int beginId = createAndRegisterNode("IfBegin", null);
		
		//get name of variable if any
		SimpleNode parExpr = (SimpleNode) node.jjtGetChild(0); 
		getNameOfVarInParExpressionStatement(parExpr);

		int conditionId = createAndRegisterNode("IfCondition", parExpr);
		int endId = createAndRegisterNode("IfEnd", null);
		

		/* Prepare information to give to children */
		Dictionary<String, String> info = new Hashtable<String, String>();
		info.put("parent", "if");
		info.put("begin", Integer.toString(beginId));
		info.put("condition", Integer.toString(conditionId));
		info.put("end", Integer.toString(endId));

		/* Get some information from eventual parent and give it to children */
		if (data instanceof Dictionary) {
			Dictionary<String, String> parentData = (Dictionary) data;
			String parent = parentData.get("parent");
			if (parent.equals("while") || parent.equals("for") || parent.equals("do")) {
				String beginLoopId = parentData.get("begin");
				String endLoopId = parentData.get("end");
				info.put("fromLoop", "true");
				info.put("loopBegin", beginLoopId);
				info.put("loopEnd", endLoopId);
			} else {
				info.put("fromLoop", "false");
			}
		} else {
			info.put("fromLoop", "false");
		}

		propagate(node, info);

		createEdgesToEnd("if", conditionId, endId, -1);
		
		return data;
	}

	public void getNameOfVarInParExpressionStatement(SimpleNode parExpr) {

		SimpleNode expr = (SimpleNode) parExpr.jjtGetChild(parExpr.jjtGetNumChildren()-1);

		visitExpression(expr); 		
	}

	@Override
	public Object visit(NoElseStatement node, Object data){
		Dictionary<String, String> parentData = (Dictionary) data;
  		int conditionifid = Integer.parseInt(parentData.get("condition"));
  		int endifid = Integer.parseInt(parentData.get("end"));
		graph.createEdge(conditionifid, endifid);
		propagate(node, data);
		return data;
	}
	@Override
  	public Object visit(ElseStatement node, Object data){
  		Dictionary<String, String> info = (Dictionary) data;
  		info.put("parent", "else");

  		int conditionifid = Integer.parseInt(info.get("condition"));		

  		/* Store and reset previous id */
  		storeAndResetPreviousId("if", conditionifid);

  		propagate(node, info);
		return data;
	}


	@Override
  	public Object visit(SwitchStatement node, Object data){
  		
  		int beginId = createAndRegisterNode("SwitchBegin", null);

  		//get name of variable if any
		SimpleNode parExpr = (SimpleNode) node.jjtGetChild(0); 
		getNameOfVarInParExpressionStatement(parExpr);

		int endId = createAndRegisterNode("SwitchEnd", null);

		setSwitchDefaultId(beginId, -1);

		Dictionary<String, String> info = new Hashtable<String, String>();
		info.put("parent", "switch");
		info.put("begin", Integer.toString(beginId));

		propagate(node, info);

		createEdgesToEnd("switch", beginId, endId, getSwitchDefaultId(beginId));

		return data;
	}
	@Override
	public Object visit(SwitchBlockStatementGroup node, Object data){
	  	Dictionary<String, String> parentData = (Dictionary) data;
  		int beginId = Integer.parseInt(parentData.get("begin"));

  		/* Store and reset previous id */
  		storeAndResetPreviousId("switch", beginId);
  				
  		String blocktype = node.jjtGetFirstToken().image;
  		if(blocktype.equals("case")) {
  			/* Create the node and edge for the SwitchCase */
  			createAndRegisterNode("SwitchCase", null);
  		} else {
  			createAndRegisterNode("SwitchDefaultCase", null);
  			setSwitchDefaultId(beginId, getId());
  		}
		propagate(node, data);
		return data;
	}


	/******* Loops Statements *******/
	@Override
  	public Object visit(WhileStatement node, Object data){
  		int beginId = createAndRegisterNode("WhileBegin", null);

		//get name of variable if any
		SimpleNode parExpr = (SimpleNode) node.jjtGetChild(0); 
		getNameOfVarInParExpressionStatement(parExpr);

		int conditionId = createAndRegisterNode("WhileCondition", parExpr);
		int endId = createAndRegisterNode("WhileEnd", null);
		graph.createEdge(conditionId, endId);
		
		isLabeledLoop(node, beginId, endId);

		Dictionary<String, String> info = new Hashtable<String, String>();
		info.put("parent", "while");
		info.put("begin", Integer.toString(beginId));
		info.put("condition", Integer.toString(conditionId));
		info.put("end", Integer.toString(endId)); 

		propagate(node, info);

		/* Add edges from WhileEnd node */
		createEdgesFromEnd("while", beginId, endId);

		return data;
	}

	@Override
 	public Object visit(ForStatement node, Object data){
 		int beginId = createAndRegisterNode("ForBegin", null);

 		//get name of variable if any
		SimpleNode forControlNode = (SimpleNode) node.jjtGetChild(0); 
 		int controlId = createAndRegisterNode("ForControl", forControlNode);

		int endId = createAndRegisterNode("ForEnd", null);
		graph.createEdge(controlId, endId);

		isLabeledLoop(node, beginId, endId);

		Dictionary<String, String> info = new Hashtable<String, String>();
		info.put("parent", "for");
		info.put("begin", Integer.toString(beginId));
		info.put("condition", Integer.toString(controlId));
		info.put("end", Integer.toString(endId)); 

		propagate(node, info);

		/* Add edges from ForEnd node */
		createEdgesFromEnd("for", beginId, endId);

		return data;
	}
	@Override
	public Object visit(DoStatement node, Object data){
  		int beginId = createAndRegisterNode("DoBegin", null);

		//get name of variable if any
		SimpleNode parExpr = (SimpleNode) node.jjtGetChild(1); 
		getNameOfVarInParExpressionStatement(parExpr);

		int conditionId = createAndRegisterNode("DoCondition", parExpr);
		int endId = createAndRegisterNode("DoEnd", null);

		isLabeledLoop(node, beginId, endId);
		
		graph.createEdge(conditionId, beginId);
		graph.createEdge(conditionId, endId);

		Dictionary<String, String> info = new Hashtable<String, String>();
		info.put("parent", "do");
		info.put("begin", Integer.toString(beginId));
		info.put("end", Integer.toString(endId)); 

		propagate(node, info);

		/* Add edges from last node to condtion node */
		createEdgesFromEnd("do", conditionId, endId);

		return data;
	}

	private void isLabeledLoop(SimpleNode node, int beginId, int endId) {
		SimpleNode parentNode = (SimpleNode) node.jjtGetParent();
		SimpleNode pparentNode = (SimpleNode) parentNode.jjtGetParent();
		if(pparentNode instanceof IdentifierStatement) {
			String label = ((SimpleNode) pparentNode.jjtGetChild(0)).jjtGetFirstToken().image;
			addIdentifier(label, beginId, endId);
		}
	}



	/******* Return, Break and Continue *******/
	@Override
	public Object visit(ReturnStatement node, Object data){
		createAndRegisterNode("ReturnStatement", (SimpleNode) node);
		graph.addReturnId(getId());

		/* check if the return is in a struture. If not put it as last id */
		isLastId(data);

		propagate(node, data);
		return data;
	}
	
	/******* Break and Continue *******/
	@Override
  	public Object visit(BreakStatement node, Object data){ return visitBreakOrContinue(node, data, "break"); }
	@Override
	public Object visit(ContinueStatement node, Object data){ return visitBreakOrContinue(node, data, "continue"); }

	public Object visitBreakOrContinue(SimpleNode node, Object data, String name) {
		String identifier = ((SimpleNode) node.jjtGetChild(0)).jjtGetFirstToken().image;

		Dictionary<String, String> info = (Dictionary) data;
		info.put("label", identifier); //add label information for labeled breaks and continues
		propagate(node, info);		

		return data;
	}

	/******* Unlabeled Break and Continue *******/
  	public Object visit(UnlabeledBreak node, Object data){
  		createAndRegisterNode("BreakStatement", null);
		return visitUnlabledBreakOrContinue(node, data, "End");
	}
	
	@Override
  	public Object visit(UnlabeledContinue node, Object data){
		createAndRegisterNode("ContinueStatement", null);
		return visitUnlabledBreakOrContinue(node, data, "Begin");
	}

	public Object visitUnlabledBreakOrContinue(SimpleNode node, Object data, String destinationName) {
		Dictionary<String, String> parentData = (Dictionary) data;
  		String parent = parentData.get("parent");
		if (parent.equals("if") || parent.equals("else")) {
			String fromLoop = parentData.get("fromLoop");
			if (fromLoop.equals("true")) { //parent of if is a while
				int loopId = Integer.parseInt(parentData.get("loop" + destinationName));
				graph.createEdge(getId(), loopId);
			} else {
				int ifId = Integer.parseInt(parentData.get(destinationName.toLowerCase()));
				graph.createEdge(getId(), ifId);
			}
		}
		
		graph.addBreakOrContinueId(getId());  // add this id to the list of the break and continue ids
		
		return data;
	}

	/******* Labeled Break and Continue *******/
	@Override
	public Object visit(LabeledBreak node, Object data){
		createAndRegisterNode("LabeledBreakStatement", null);
		return visitLabeledBreakOrContinue(node, data, 1);
	}

	@Override
	public Object visit(LabeledContinue node, Object data){
		createAndRegisterNode("LabeledContinueStatement", null);
		return visitLabeledBreakOrContinue(node, data, 0);
	}

	public Object visitLabeledBreakOrContinue(SimpleNode node, Object data, int nb) {
		Dictionary<String, String> parentData = (Dictionary) data;
		
  		String label = parentData.get("label");   //get label from Break or Continue Statement
		
		if(!identifiersContains(label)) {   //if the label does not exists as identifier for a loop
			System.out.println("Error : labeled break or continue but no labeled loop in " + fileNumber + "_" + methodId);
			String destinationName;
			if (nb==0) { destinationName = "Begin"; } else { destinationName = "End";}
			return visitUnlabledBreakOrContinue(node, data, destinationName);
		}
  		int id = getIdentifier(label).get(nb);  //0 for continue is the id of the beginning, 1 for break is the id of the end
  		graph.createEdge(getId(), id);

		graph.addBreakOrContinueId(getId());  // add this id to the list of the break and continue ids

  		return data;
	}


  	

	/******* Simple Statements *******/
	@Override
	public Object visit(LocalVariableDeclarationStatement node, Object data){ 
		// get name of variable for tp4 and tp5
		String varName = ((SimpleNode)node.jjtGetChild(node.jjtGetNumChildren()-1)).jjtGetFirstToken().image;
		//System.out.println("------------ Local Var Decl " + varName);

		visitSimpleStatement(data, "VariableDeclaration", (SimpleNode) node);

		graph.putVar(varName, this.currentNode); //tp4
		graph.putDef(this.currentNode, varName);  //tp5 - DD
		verifyIfDefAndUseNode(node, varName);

		propagate(node, data);  // needed for Expression nodes to get possible variable names
		return data; 
	}
	@Override
	public Object visit(EmptyStatement node, Object data){ visitSimpleStatement(data, "EmptyStatement", null); return data;}
	@Override
  	public Object visit(StatementExpression node, Object data){ 
  		visitSimpleStatement(data, "StatementExpression", (SimpleNode) node); 


  		propagate(node, data);  // needed for Expression nodes to get possible variable names

  		return data;
  	}
  	@Override
	public Object visit(Expression node, Object data){ 
		if(!(node.jjtGetParent() instanceof ParExpression) && !(node.jjtGetParent().toString().contains("For"))) { // if not from : if, while, switch, do... or for
			visitExpression((SimpleNode) node);
		}
  		return data;
	}	

	private void visitExpression(SimpleNode expr) {
  		if(expr.jjtGetNumChildren()==2) {//assignment
  			String varName = getVarName(expr);
  			verifyIfDefAndUseNode(expr, varName);
  		} else {
  			SimpleNode expr1 = (SimpleNode) expr.jjtGetChild(0);
  			if(expr1.jjtGetNumChildren()==2) {  //assignment by structure ? : 
	  			String varName = getVarName(expr);
	  			verifyIfDefAndUseNode(expr, varName);
  			} else { //assignement with ++, --, ...
  				SimpleNode expr2 = (SimpleNode) expr1.jjtGetChild(0);
  				SimpleNode expr3 = (SimpleNode) expr2.jjtGetChild(0);
  				if((SimpleNode) expr3.jjtGetChild(0) instanceof PrefixOp) {
  					getVarName((SimpleNode) expr3.jjtGetChild(1));
  					graph.putDefAndUseNode(this.currentNode);  //tp5 - DD
  				}  else if(expr3.jjtGetNumChildren() > 1) {
  					if((SimpleNode) expr3.jjtGetChild(1) instanceof PostfixOp) {
	  					getVarName((SimpleNode) expr3.jjtGetChild(0));
	  					graph.putDefAndUseNode(this.currentNode);  //tp5 - DD
	  				}
  				}
  			}
  		}
	}
	private String getVarName(SimpleNode expr) {
		String varName = expr.jjtGetFirstToken().image;
		if(varName.equals("this")) {
			varName = expr.jjtGetFirstToken().next.next.image;
		}
  		graph.putVar(varName, this.currentNode);
  		graph.putDef(this.currentNode, varName);  //tp5 - DD
  		return varName;
	}

	private void verifyIfDefAndUseNode(SimpleNode node, String var) {
		/* checkif defined variable is used at the same time it is defined such as in a = a+1; or a-=1;*/
		// get assignement operator
		SimpleNode exprrest = (SimpleNode) node.jjtGetChild(1);
		SimpleNode assignementop = (SimpleNode) exprrest.jjtGetChild(0);
		Token assignement = assignementop.jjtGetFirstToken();
		if(assignement.kind == 61) {  //a ssignement with = , def and use if variable appears twice 
			Token token = node.jjtGetFirstToken();
			int count = 0;
			while(token !=null && token != node.jjtGetLastToken()) {
				if(token.image.equals(var)) {
					count++;
				}
				if(count>1) {
					graph.putDefAndUseNode(this.currentNode);   // if variable also used then it is added to defandusenode
				}
				token = token.next;
			}
			//last iteration for the last token
			if(token.image.equals(var)) {
				count++;
			}
			if(count>1) {
				graph.putDefAndUseNode(this.currentNode);  
			}

		} else if(assignement.kind == 85 || assignement.kind == 86 ||assignement.kind == 87 ||assignement.kind == 88 ||
			assignement.kind == 89 ||assignement.kind == 90 ||assignement.kind == 91 ||assignement.kind == 92 ||
			assignement.kind == 93 ||assignement.kind == 94 ||assignement.kind == 95 ) {  //assignment with += -= *= /= &= |=  ^= %= <<= >>= >>>=
			graph.putDefAndUseNode(this.currentNode);
		}
	}

	public void visitSimpleStatement(Object data, String name, SimpleNode node) {
		createAndRegisterNode(name, node);
		/* Set this node as the current last node if it is not in one of the structures if, else, while, for or switch */
		isLastId(data);
	}


	/******* Override Constructor Visit so that it does not propagate *******/
	@Override
	public Object visit(ConstructorDecl node, Object data){ return data; }


	/******* Other functions *******/
	private void createEdgeRegisterPredecessor(int predId, int childId) {
		graph.createEdge(predId, childId);

	}

	public int createAndRegisterNode(String name, SimpleNode node) {
		/* This function creates the new node and add an edge between this node and the previous one */
		this.currentNode = new CfgNode(getNewId(), name);
		this.currentNode.putAstNode(node);
		graph.addNode(this.currentNode);
		if (!name.contains("End") && !name.equals("DoCondition")) {  // if it is not IfEnd, WhileEnd, ForEnd, SwitchEnd neither DoCondition
			if(!name.equals("Entry")) { // if not first node, create link with previous one
				graph.createEdge(getPreviousId(), getId());
			}
			setPreviousId(getId()); // set this node id as the previous id
		}
		return getId();  //return the id of the node
	}


	public void storeAndResetPreviousId(String name, int beginOrCondId) {
	  	/* Store information : id of the last statement of the last case part is switch or id of the last statement of if  if else */
  		int previousIdIf = getPreviousId();  
  		if(name.equals("if") || previousIdIf != beginOrCondId) { //SWITCH :for the first "case", previousId is beginId but no edge needed between begin and end switch nodes
  			setIdInfo(beginOrCondId, previousIdIf);
  		}
  		
  		/* Reset previous id to the one of IfCondition if if-else or BeginSwitch if switch */
  		setPreviousId(beginOrCondId);
  	}

	public void createEdgesToEnd(String struct, int beginOrCondId, int endId, int defaultSwitchId) {
		/* Add edges to End node for If and Switch after propagate */
		ifSwitchLastEdges(struct, beginOrCondId, endId, defaultSwitchId, getPreviousId());
		if(containsKey(beginOrCondId)) {  //there are several cases
			for(Integer id : getIdInfo(beginOrCondId)) {
				ifSwitchLastEdges(struct, beginOrCondId, endId, defaultSwitchId, id);
			}
		}
		/* Update the current last id of the graph and set the previous id to endId */
		graph.setCurrentLastid(endId);
		setPreviousId(endId);
	}
	public void ifSwitchLastEdges(String struct, int beginOrCondId, int endId, int defaultSwitchId, int currentId) {
		if (struct.equals("switch") && !graph.idIsBreakOrContinue(currentId) && defaultSwitchId!=-1) {
			graph.createEdge(currentId, defaultSwitchId);  //create edge with defaultSwitchId
		} else if(struct.equals("switch") || struct.equals("if") && ! graph.idIsBreakOrContinue(currentId)) {  //if previous id is not a break - in case it is an If
			graph.createEdge(currentId, endId);
		} 
	}

	public void createEdgesFromEnd(String struct, int beginOrCondId, int endId) {
		/* Add edges from last node for For, Do and While after propagate */
		if (!graph.idIsBreakOrContinue(getPreviousId())){  // if previous id is not a break nor a continue then create edge from last node to Begin node
			graph.createEdge(getPreviousId(), beginOrCondId);  
		}
		graph.setCurrentLastid(endId);
		setPreviousId(endId);
	}

	public void isLastId(Object data) {
		/* Check if it is not inside a struture. If not, set it as the last id  */
		if (data instanceof Dictionary) {
			Dictionary<String, String> parentData = (Dictionary) data;
			String parent = parentData.get("parent");
			if (! parent.equals("while") && ! parent.equals("if") && ! parent.equals("else") && ! parent.equals("for") && ! parent.equals("switch")) { // if not inside structure
				graph.setCurrentLastid(getId());
			}
		} else { // no structure as parent
			graph.setCurrentLastid(getId());
		}
	}



	/******* To write the results *******/
	public void writeResults() {
		writeCfgResults();
		if(TP_NUMBER == 4 || TP_NUMBER == 5) {
			writeTp4Results();
			if(TP_NUMBER == 5) { writeTp5Results(); }
		}
		endTime = System.nanoTime();
		long duration = endTime - startTime;
    	System.out.println("Elapsed time for method " + this.fileNumber + "_" + this.methodId + "_" + this.methodName + " (TP" + TP_NUMBER + "): " + duration/1000000 + " ms");
	}
	
	public void writeCfgResults() {
		CfgWriter cfgwriter = new CfgWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp3_cfg.dot");
		cfgwriter.writeBeginning("CFG : " + this.methodName); 	
		cfgwriter.writeGraphNodesAndEdges(graph);
		cfgwriter.writeEnd(); 
	} 

	
	public void writeTp4Results() {
		writeDominatorsResults();
		writePostDominatorsResults();
		writeReachingDef();
	}

	public void writeDominatorsResults() {
		DominatorsTree domTree = new DominatorsTree(graph);
		HashMap<CfgNode, CfgNode> directDoms = domTree.getDirectDoms();
		DominatorsWriter domwriter = new DominatorsWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp4_dom.dot");
        domwriter.writeBeginning(this.methodName + " : Arbre des Dominateurs"); 
		domwriter.writeGraphNodes(graph.getNodes());
		domwriter.writeGraphEdges(directDoms);
		domwriter.writeEnd(); 
	}  

	public void writePostDominatorsResults() {
		CfgGraphReverse reversegraph = new CfgGraphReverse(graph.getName(), graph);
		reversegraph.setNodes();

		DominatorsTree domTree = new DominatorsTree(reversegraph);
		directPostDoms = domTree.getDirectDoms(); 
		postdominatorsMap = domTree.dom;  //for TP5

		DominatorsWriter postdomwriter = new DominatorsWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp4_postdom.dot");
        postdomwriter.writeBeginning(this.methodName + " : Arbre des Postdominateurs"); 
		postdomwriter.writeGraphNodes(reversegraph.getNodes());
		postdomwriter.writeGraphEdges(directPostDoms);
		postdomwriter.writeEnd(); 
	}

	public void writeReachingDef() {
		ReachingDef rdef = new ReachingDef(graph);
		CfgGraph graphWithRdef = rdef.getOut();
		inReachingDef = rdef.in;  //for TP5
		RdefWriter rwriter = new RdefWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp4_rdef.dot");
		rwriter.writeBeginning(this.methodName + " : CFG avec définitions valides"); 	
		rwriter.writeGraphNodesAndEdges(graphWithRdef);
		rwriter.writeEnd(); 
	}

	public void writeTp5Results() {
		ArrayList<CfgNode[]> cd = writeControlDependencies();
		ArrayList<CfgNode[]> dd = writeDataDependencies();
		writePDGGraph(cd, dd);
		writeSlicing(cd, dd);
	}

	public ArrayList<CfgNode[]> writeControlDependencies(){
		ControlDependencies controlDep = new ControlDependencies(graph, postdominatorsMap, directPostDoms);
		ArrayList<CfgNode[]> cd = controlDep.computeCD();
		DependenciesWriter writer = new DependenciesWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp5_CD.dot");
        writer.writeBeginning(this.methodName + " : Dépendance de Contrôle"); 
		writer.writeGraphNodes(graph.getNodes());
		writer.writeGraphEdges(cd);
		writer.writeEnd(); 
		return cd;
	}

	public ArrayList<CfgNode[]> writeDataDependencies(){
		print("var " + graph.getVar().toString());
		print("def " + graph.getDef().toString());
		print("def and use " + graph.getDefAndUseNodes().toString());
		DataDependencies dataDep = new DataDependencies(graph, inReachingDef);
		ArrayList<CfgNode[]> dd = dataDep.computeDD();
		DependenciesWriter writer = new DependenciesWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp5_DD.dot");
        writer.writeBeginning(this.methodName + " : Dépendance de Données"); 
		writer.writeGraphNodes(graph.getNodes());
		writer.writeGraphEdges(dd);
		writer.writeEnd(); 
		return dd; 
	}

	public void writePDGGraph(ArrayList<CfgNode[]> cd, ArrayList<CfgNode[]> dd){
		DependenciesWriter writer = new DependenciesWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp5_PDG.dot");
        writer.writeBeginning(this.methodName + " : Graphe des Dépendances de Programme"); 
		writer.writeGraphNodes(graph.getNodes());
		writer.writeGraphEdges(cd);
		writer.writeGraphEdges(dd);
		writer.writeEnd(); 
	}

	public void writeSlicing(ArrayList<CfgNode[]> cd, ArrayList<CfgNode[]> dd){
		Slicing slicing = new Slicing(this.graph, cd, dd);
		HashMap<String, ArrayList<CfgNode>> s = slicing.getSlicingForAllVar();
		SlicingWriter writer = new SlicingWriter("./cfg/dotfiles/" + this.fileNumber + "_" + this.methodId + "_" + this.methodName + "_tp5_slicing.dot");
		writer.writeBeginning(this.methodName + " : Slicing"); 
		writer.writeGraphNodes(s);
		writer.writeEnd(); 
	}


	private void reset() {
		this.id = -1;
		this.currentPreviousId = -1;
		this.idInfo.clear(); 
		this.identifiers.clear(); 
		this.switchDefaultId.clear();
		this.methodId++;
	}

	private void print(String s) {
		System.out.println("-----------------" + s);
	}
}
