package javaparser;

import java.util.ArrayList;
import java.util.HashSet;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MetricsVisitor extends AbstractVisitor
{
  int count_if = 0;  //instructions conditionnelles : if, else, switch
  int count_while = 0;  //instructions de répétition : while, for
  int count_break = 0;  //instructions de saut : break, continue, return
  int count_localvar = 0;  //variables locales
  String className = "name";

  boolean firstMethod = true;

  ArrayList<String> classNames = new ArrayList<String>();
  ArrayList<String> methodNames = new ArrayList<String>();
  ArrayList<Integer> ifStats = new ArrayList<Integer>();
  ArrayList<Integer> whileStats = new ArrayList<Integer>();
  ArrayList<Integer> breakStats = new ArrayList<Integer>();
  ArrayList<Integer> localvarStats = new ArrayList<Integer>();


  public Object visit(IfStatement node, Object data){
  	    count_if ++;
		propagate(node, data);
		return data;
	}
  public Object visit(ElseStatement node, Object data){
  		count_if ++;
		propagate(node, data);
		return data;
	}
  public Object visit(SwitchStatement node, Object data){
  	    count_if ++;
		propagate(node, data);
		return data;
	}

  public Object visit(WhileStatement node, Object data){
  	    count_while ++;
		propagate(node, data);
		return data;
	}
  public Object visit(ForStatement node, Object data){
  	    count_while ++;
  	    propagate(node, data);
		return data;
	}

  public Object visit(BreakStatement node, Object data){
  	    count_break ++;
		propagate(node, data);
		return data;
	}
  public Object visit(ContinueStatement node, Object data){
  	    count_break ++;
		propagate(node, data);
		return data;
	}
  public Object visit(ReturnStatement node, Object data){
  		count_break ++;
		propagate(node, data);
		return data;
	}

  public Object visit(LocalVariableDeclarationStatement node, Object data){
  	    count_localvar ++;
		propagate(node, data);
		return data;
	}

  public Object visit(ClassDeclaration node, Object data){
		Token token = node.jjtGetFirstToken();
		while (!token.image.equals("class")) { 
			token = token.next;
		}
		className = token.next.image;  //the name of the class is after the keyword "class"

		propagate(node, data);
		return data;
	}

  public Object visit(MethodOrFieldDecl node, Object data){
		Token token = node.jjtGetFirstToken();
		int beginLineFirstToken = token.beginLine; 
		Token tokenNext = token.next; 

		while (tokenNext != null) {
			if (tokenNext.kind == 96 && tokenNext.beginLine == beginLineFirstToken) {
				String methodName = token.image;
				collectClassName();
				collectMethodName(methodName);
				
				if (!firstMethod) {
					collectIfStatistics();
					collectWhileStatistics();
					collectBreakStatistics();
					collectLocalvarStatistics();
				}
				firstMethod = false;
				break;
			}
			token = tokenNext;
			tokenNext = token.next; 
		}
		propagate(node, data);
		return data;
	}

  public Object visit(VoidMethodDecl node, Object data){
		Token token = node.jjtGetFirstToken();
		while (!token.image.equals("void")) {
			token = token.next;
		}
		String methodName = token.next.image;
		collectClassName();
		collectMethodName(methodName);
		if (!firstMethod) {
			collectIfStatistics();
			collectWhileStatistics();
			collectBreakStatistics();
			collectLocalvarStatistics();
		}
		firstMethod = false;
		propagate(node, data);
		return data;
	}

  public void collectMethodName(String methodName) {
  	methodNames.add(methodName);
  }

  public void collectClassName() {
  	classNames.add(className);
  }

  public void collectIfStatistics() {
  	ifStats.add(count_if);
  	count_if = 0; 
  }

  public void collectWhileStatistics() {
  	whileStats.add(count_while);
  	count_while = 0; 
  }
  public void collectBreakStatistics() {
  	breakStats.add(count_break);
  	count_break = 0; 
  }

  public void collectLocalvarStatistics() {
  	localvarStats.add(count_localvar);
  	count_localvar = 0; 
  }

  public JSONObject JSONResults() {
  	/* collecte des dernières valeurs */
  	collectIfStatistics();
  	collectWhileStatistics();
	collectBreakStatistics();
	collectLocalvarStatistics();

	JSONObject fileObj = new JSONObject();

	
	ArrayList<String> classNoDoubles = new ArrayList();
	for(String classname : classNames){
		if(!classNoDoubles.contains(classname)){
			classNoDoubles.add(classname);
		}
	}

	int i = 0;
	for(String name : classNoDoubles) {
		JSONObject classObj = new JSONObject();
		for(int j = 0 ; j < methodNames.size() ; j++){
			if(classNames.get(j).equals(name)){
				JSONObject methodObj = new JSONObject();

				methodObj.put("#IF", Integer.toString(ifStats.get(j)));
				methodObj.put("#WHILE", Integer.toString(whileStats.get(j)));
				methodObj.put("#BREAK", Integer.toString(breakStats.get(j)));
				methodObj.put("#VARIABLES_LOCALES", Integer.toString(localvarStats.get(j)));

				classObj.put("Methode " + j + " : " + methodNames.get(j), methodObj);
			}
		}
		fileObj.put("Classe " + i + " : " + name, classObj);
		i++;
	}
	return fileObj; 
	
  
  } 



}
