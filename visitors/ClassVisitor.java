package javaparser;


import java.io.FileWriter;
import java.io.IOException;
import java.util.*; 

public class ClassVisitor extends AbstractVisitor
{
	  	
	public Object visit(ClassDeclaration node, Object data){		
		Token token = node.jjtGetFirstToken();
		while (!token.image.equals("class")) { 
			token = token.next;
		}
		Token className = token.next;
		if (className.kind == 97 || className.kind == 102 || className.kind == 103 || className.kind == 104) { //if it is ) or ; or , or .
			return data;
		}
		
		String currentClassName = verifyClassName(className.image); 
		writeClass(currentClassName);
		
		propagate(node, data);
		return data;
	}

	private String verifyClassName(String name) {
		//the name of a class can't be Node or Edge because it causes a problem with GraphViz, so Class is appended
		if (name.equals("Node")) {
			return "NodeClass";
		} else if (name.equals("Edge")) {
			return "EdgeClass";
		}
		return name;
	}

	
	public void writeClass(String className) { //to class in text file
		try {
	        FileWriter file = new FileWriter("uml/classNamesList.txt", true);
	        file.write(className + "\n");
	        file.flush();
      	} catch (IOException e) {
            e.printStackTrace();
        }
	}

}
