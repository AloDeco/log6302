JJTREE=jjtree.bat
JAVACC=javacc.bat
JJDOC=jjdoc.bat
JAVAC=javac
GRAMMAR=java1_7
OUTPUT=javaparser


.PHONY: compile clean mrproper run all uml cfg

all: tp3

tp1: clean compile runtp1

tp2: clean compile runtp2 uml

tp3: clean compile runtp3 cfg

tp4: clean compile runtp4 cfg

tp5: clean compile runtp5 cfg


test: test3

test1: clean compile runttest1

test2: clean compile runttest2 uml
	
test3: clean compile runttest3 cfg



classnames: clean cleanclassnames compile runclassnames

classnamestest: clean cleanclassnames compile runclassnamestest

compile:
	mkdir -p $(OUTPUT)
	$(JJTREE) -OUTPUT_DIRECTORY:$(OUTPUT) $(GRAMMAR).jjt
	$(JAVACC) -OUTPUT_DIRECTORY:$(OUTPUT) $(OUTPUT)/$(GRAMMAR).jj
	$(JJDOC) -OUTPUT_DIRECTORY:$(OUTPUT) $(OUTPUT)/$(GRAMMAR).jj
	cp -rf visitors/*.java $(OUTPUT)
	cp -rf uml/*.java $(OUTPUT)
	cp -rf cfg/*.java $(OUTPUT)
	cp -rf cfg/tp3/*.java $(OUTPUT)
	cp -rf cfg/tp4/*.java $(OUTPUT)
	cp -rf cfg/tp5/*.java $(OUTPUT)
	$(JAVAC) $(OUTPUT)/*.java

compiletest:
	$(JAVAC) @test/filelist.txt

compiletp3:
	$(JAVAC) @apache_beam/filelist.txt

clean:
	rm -rf $(OUTPUT)
	rm -rf uml/inheritance/*.txt
	rm -rf uml/composition/*.txt
	rm -rf uml/dotfiles/*.dot
	rm -rf uml/graphs/*.pdf
	rm -rf cfg/dotfiles/*.dot
	rm -rf cfg/graphs/*.pdf

cleanclassnames:
	rm -f ./uml/classNamesList.txt

mrproper: clean
	rm -rf *~ $(GRAMMAR).html

runtp1: 
	java javaparser.JavaParser1_7 @apache_beam/filelist.txt 1

runtp2: 
	java javaparser.JavaParser1_7 @apache_beam/filelist.txt 2 

runtp3: 
	java javaparser.JavaParser1_7 @tp3_methods/filelist.txt 3 

runtp4: 
	java javaparser.JavaParser1_7 @tp3_methods/filelist.txt 4 

runtp5: 
	java javaparser.JavaParser1_7 @tp3_methods/filelist.txt 5 


runttest: 
	java javaparser.JavaParser1_7 @test/filelist.txt

runttest1: 
	java javaparser.JavaParser1_7 @test/filelist.txt 1

runttest2: 
	java javaparser.JavaParser1_7 @test/filelist.txt 2

runttest3: 
	java javaparser.JavaParser1_7 @test/filelist.txt 3


runclassnames:
	java javaparser.JavaParser1_7 @apache_beam/filelist.txt 21

runclassnamestest:
	java javaparser.JavaParser1_7 @test/filelist.txt 21


uml:
	py ./uml/createUmlDiagrams.py

cfg:
	py ./cfg/createCfg.py