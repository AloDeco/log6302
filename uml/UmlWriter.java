package javaparser;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*; 
import java.io.File; 
import java.io.FileNotFoundException; 
import java.util.Scanner; 

public class UmlWriter {

	FileWriter file; //uml file

	public UmlWriter(String fileName) {
		try {
		    this.file = new FileWriter(fileName, true);
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	public void writeBeginning() {
		try {
			file.write("digraph G {\n");
			file.write("fontname = \"Courier\"\n"); 
			file.write("node [\n\tfontname = \"Courier\"\n\tshape = \"record\"\n]\n");
			file.write("edge [minlen = \"1\"] \n"); 
			
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

	
	public void writeFile( Set<String> classNamesAll, ArrayList<String> classNamesForMethods, ArrayList<String> methodNames, ArrayList<String> methodtype, ArrayList<String> methodArgs, ArrayList<String> classNamesForFields, ArrayList<String> fields) throws IOException {

    	for(String name : classNamesAll) {

    		file.write(name + " [ label = \"{" + name + "|");

			for(int j = 0 ; j < fields.size() ; j++){  //for the fields
				if(classNamesForFields.get(j).equals(name)){
					file.write("-" + fields.get(j) + "\\l");
				}
			}
			file.write("|");


			for(int j = 0 ; j < methodNames.size() ; j++){  //for the methods
				if(classNamesForMethods.get(j).equals(name)){
					file.write("+" + methodNames.get(j) + "(" + methodArgs.get(j) +") : " + methodtype.get(j) + "\\l");
				}
			}
			file.write("}\" ]\n");
			file.flush();
		}
	}

	public void writeInheritance(String inheritanceFileName) {
		//file with inheritance relations
		File inheritanceFile = new File(inheritanceFileName);
		if (!inheritanceFile.exists()) {return;}
		Set<String> her = new HashSet<String>();
		her = deleteDuplicatesInFile(inheritanceFile);
	            	
	    try {
			file.write("edge [ arrowhead = \"empty\" ]\n");
			for (String rel : her) {
			   	file.write(rel + "\n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        } 
	}


	public void writeComposition(String compositionFileName) {
		//file with compositions relations
		File compositionFile = new File(compositionFileName);
		if (!compositionFile.exists()) {return;}
		Set<String> comp = new HashSet<String>();
		comp = deleteDuplicatesInFile(compositionFile);
		
		try {
			file.write("edge [ arrowhead = \"diamond\" ]\n");
			for (String rel : comp) {
			    file.write(rel + "\n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        } 
	}
	

    public void writeEnd() {
    	try {
			file.write("}");
			file.flush();
			file.close();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	

	public Set<String> deleteDuplicatesInFile(File fileName) {
		Set<String> set = new HashSet<String>();
		
	    try {
	    	Scanner relReader = new Scanner(fileName);
			while (relReader.hasNextLine()) {
				String data = relReader.nextLine();
				set.add(data);
			}
			relReader.close();
		} catch (FileNotFoundException e) {
            e.printStackTrace();
        } 
		
		return set;
	}
	

}