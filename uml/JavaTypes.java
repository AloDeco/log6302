package javaparser;

import java.util.*; 
import java.io.*;

public class JavaTypes {

  	public Set<String> classes = new HashSet<String>();
	public JavaTypes() {
		File classFile = new File("uml/classNamesList.txt");
		try {
	    	Scanner classReader = new Scanner(classFile);
			while (classReader.hasNextLine()) {
				String data = classReader.nextLine();
				classes.add(data);
			}
			classReader.close();
		} catch (FileNotFoundException e) {
            e.printStackTrace();
        } 
	}


}