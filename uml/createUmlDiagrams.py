import os
import timeit

start = timeit.default_timer()

for file in os.listdir("./uml/dotfiles/"):
    if file.endswith(".dot"):
        dotfile = "./uml/dotfiles/" + file
        pdfgraph = "./uml/graphs/" + file[:-4] + ".pdf"
        os.system("dot -T pdf -o "+ pdfgraph +" " + dotfile)

stop = timeit.default_timer()

duration = round(stop - start, 3)

print("Created all uml diagrams in " + str(duration) + " secondes")