package javaparser;

import java.util.*; 
import java.io.*; 

public class CfgWriter extends Writer{

	public CfgWriter(String fileName) {
		super(fileName);
	}

	public void writeGraphNodesAndEdges(CfgGraph graph) {
		writeGraphNodes(graph.getNodes());
		writeGraphEdges(graph.getEdges());
	}

	public void writeGraphNodes(ArrayList<CfgNode> nodes) {	
		try {
			for (CfgNode node : nodes) {
				file.write(node.getId() + " [ label = \" " + node.getId() + " - " + node.getName() + " \" ] \n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void writeGraphEdges(ArrayList<CfgEdge> edges) {	
		/* Remove duplicates */
        Set<String> relations = new LinkedHashSet<String>();
        for (CfgEdge edge : edges) {
        	relations.add(edge.getRelation());
        } 

		try {
			for (String rel : relations) {
				file.write(rel + " \n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

}