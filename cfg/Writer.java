package javaparser;

import java.util.*; 
import java.io.*; 

public class Writer {

	FileWriter file; 

	public Writer(String fileName) {
		try {
		    this.file = new FileWriter(fileName, true);
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	public void writeBeginning(String title) {
		writeGraphOpening();
		writeTitle(title);
	}
	
	public void writeGraphOpening() {
		try {
			file.write("digraph G {\n");
			file.write("node [\n\tfontname = \"Courier\"\n\tshape = \"ellipse\"\n]\n");			
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void writeTitle(String title) {
		try {
			file.write("fontname = \"Arial\"\n"); 
			file.write("fontsize=30\n");
			file.write("labelloc=\"t\"\n");
			file.write("label=\"" + title + "\" \n"); 
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

    public void writeEnd() {
    	try {
			file.write("}");
			file.flush();
			file.close();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	

}