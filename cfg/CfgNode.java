package javaparser;

import java.util.*; 
import java.io.*; 

public class CfgNode {

	private int id;
	private String name;
	private ArrayList<CfgNode> predecessors = new ArrayList<CfgNode>();
	private CfgNode gen;
	private ArrayList<CfgNode> kill = new ArrayList<CfgNode>();
	private ArrayList<CfgNode> in = new ArrayList<CfgNode>();
	private ArrayList<CfgNode> out = new ArrayList<CfgNode>();
	private SimpleNode astnode;
	private ArrayList<CfgNode> pdpredecessors = new ArrayList<CfgNode>(); //tp5


	public CfgNode(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void addPredecessor(CfgNode predecessor) {
		predecessors.add(predecessor);
	}

	public ArrayList<CfgNode> getPredecessors() {
		return this.predecessors;
	}

	public boolean isChildOf(CfgNode node) {
		if(predecessors.contains(node)) {
			return true;
		}
		return false;
	}

	public void setGen(CfgNode gen) {
		this.gen = gen;
	}
	public void setKill(ArrayList<CfgNode> kill) {
		this.kill = kill;
	}
	public void setIn(ArrayList<CfgNode> in) {
		this.in = in;
	}
	public void setOut(ArrayList<CfgNode> out) {
		this.out = out;
	}
	public CfgNode getGen() {
		return this.gen;
	}
	public ArrayList<CfgNode> getKill() {
		return this.kill;
	}
	public ArrayList<CfgNode> getIn() {
		Collections.sort(in, new Comparator<CfgNode>() {
       		public int compare(CfgNode n1, CfgNode n2) {
            return new Integer(n1.getId()).compareTo(new Integer(n2.getId()));
        	}
    	});
		return this.in;
	}
	public ArrayList<CfgNode> getOut() {
		Collections.sort(out, new Comparator<CfgNode>() {
       		public int compare(CfgNode n1, CfgNode n2) {
            return new Integer(n1.getId()).compareTo(new Integer(n2.getId()));
        	}
    	});
		return this.out;
	}

	public void putAstNode(SimpleNode node) {
		this.astnode = node;
	}
	public SimpleNode getAstNode() {
		return this.astnode;
	}

	public void addPDPredecessor(CfgNode node) {
		this.pdpredecessors.add(node);
	}
	public ArrayList<CfgNode> getPDPredecessors() {
		return this.pdpredecessors;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
                return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        
        }
		
		CfgNode node = (CfgNode) o;
		return name.equals(node.getName()) && id == node.getId();
		
	}

	@Override
	public String toString(){
		return ""+id;
	}

	@Override
  	public int hashCode() {
    	return id;
  	}
	
}