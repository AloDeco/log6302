package javaparser;

import java.util.*; 
import java.io.*; 

// Source : https://mkyong.com/java/how-to-execute-shell-command-from-java/

public class CheckIfCompile {

	String fileName;

	public CheckIfCompile(String fileName) {
		this.fileName = fileName;
	}

	public boolean checkErrors() {
		ProcessBuilder processBuilder = new ProcessBuilder();
		processBuilder.command("javac ", this.fileName);

		try {
			Process process = processBuilder.start();

			StringBuilder output = new StringBuilder();

			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

			String line;
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}

			int exitVal = process.waitFor();
			if (exitVal == 0) {
				return false;
			} else {
				System.out.println("_____________________________________________________________________");
				System.out.println("ERROR : the given code in file " + fileName + " does not compile :");
				System.out.println(output);
				System.out.println("_____________________________________________________________________");
				return true;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return false; 

	} 
	
	

}