package javaparser;

import java.util.*; 
import java.io.*; 

public class CfgGraph {

	protected String name;
	protected ArrayList<CfgNode> nodes = new ArrayList<CfgNode>();
	protected ArrayList<CfgEdge> edges = new ArrayList<CfgEdge>();
	protected int lastid;
	protected ArrayList<Integer> returnIds = new ArrayList<Integer>();  //id of return nodes : needs to be linked with Exit node 
	protected ArrayList<Integer> breakAndContinueIds = new ArrayList<Integer>();  //id of break and continue nodes

	protected HashMap<String, ArrayList<CfgNode>> var = new HashMap<String, ArrayList<CfgNode>>();
	protected HashMap<CfgNode, String> vardef = new HashMap<CfgNode, String>();
	protected ArrayList<CfgNode> defandusenode = new ArrayList<CfgNode>();

	

	public CfgGraph(String name) {
		this.name = name;
		this.lastid = 0;
	}
	public CfgGraph(String name, ArrayList<CfgNode> nodes, ArrayList<CfgEdge> edges) {
		this.name = name;
		this.nodes = nodes;
		this.edges = edges;
	}
	public String getName() { return this.name; }

	public void addNode(CfgNode node) { nodes.add(node); }
	public ArrayList<CfgNode> getNodes() { return this.nodes; }

	public void addEdge(CfgEdge edge) { edges.add(edge); }
	public ArrayList<CfgEdge> getEdges() { return this.edges; }

	public void setCurrentLastid(int id) { this.lastid = id; }
	public void addReturnId(int id) { this.returnIds.add(id); }
	public boolean returnIdsContain(int id) { return this.returnIds.contains(id); }

	public void addBreakOrContinueId(int id) { this.breakAndContinueIds.add(id); }
	public boolean idIsBreakOrContinue(int id) { return this.breakAndContinueIds.contains(id); }

	public HashMap<String, ArrayList<CfgNode>> getVar() { return this.var; }
	public void putVar(String v, CfgNode node) {
		ArrayList<CfgNode> list;
		if (this.var.containsKey(v)) {
			list = this.var.remove(v);
		} else {
			list = new ArrayList<CfgNode>();
		}
		list.add(node);
		this.var.put(v, list);
	}

	public HashMap<CfgNode, String> getDef() { return this.vardef; }
	public void putDef(CfgNode node, String v) {
		this.vardef.put(node, v);
	}

	public ArrayList<CfgNode> getDefAndUseNodes() { return this.defandusenode; }
	public void putDefAndUseNode(CfgNode node) {
		this.defandusenode.add(node);
	}


	public int getNodeIndiceById(int id) {
		for (int i = 0; i< nodes.size(); i++) {
			if (nodes.get(i).getId() == id) {
				return i; 
			}
		}
		return -1;
	}


	public void createEdge(int previousid, int nextid) {
		if(!returnIdsContain(previousid)) { // if the previous id is the one of a return Statement, don't create an edge
			addEdge(new CfgEdge(previousid, nextid));
		}
		addPredecessor(previousid, nextid);
	}

	public void addPredecessor(int previousid, int nextid) {
		int currentNodeIndice = getNodeIndiceById(nextid);
		int previousNodeIndice = getNodeIndiceById(previousid);

		nodes.get(currentNodeIndice).addPredecessor(nodes.get(previousNodeIndice));  //add a predecessor to the current node
	}

	public void createEdgeFromReturn(int previousid, int nextid) {
		addEdge(new CfgEdge(previousid, nextid));
		addPredecessor(previousid, nextid);
	}

	public void createLastRelation() {
		CfgNode lastNode = nodes.get(nodes.size()-1);
		int lastAttributedId = lastNode.getId();
		CfgNode exitNode = new CfgNode(lastAttributedId + 1, "Exit");
		nodes.add(exitNode);

		createEdgeFromReturn(this.lastid, lastAttributedId+1);
		for (int id : returnIds) {
			createEdgeFromReturn(id, lastAttributedId+1);
		}	
	}

	public void verifyGraph() {
		ArrayList<CfgNode> nodescopy = new ArrayList<CfgNode>(this.nodes);
		for(int i = 1; i<nodescopy.size(); i++) {
			CfgNode node = nodescopy.get(i);
			if(node.getPredecessors().size()==0) {
				nodes.remove(node);
				CfgEdge edge = getEdgeByOneId(node.getId());
				while(edge != null) {
					int othernodeid = edge.getChildId();
					removeParent(othernodeid, node.getId());
					edges.remove(edge);
					edge = getEdgeByOneId(node.getId());
				}
			}
		}
	}

	private CfgEdge getEdgeByOneId(int id) {
		for(CfgEdge edge : edges) {
			if(edge.getPredId() == id || edge.getChildId() == id) {
				return edge;
			}
		}
		return null;
	}
	private void removeParent(int childId, int predId) {
		CfgNode node = nodes.get(getNodeIndiceById(childId));
		ArrayList<CfgNode> preds = new ArrayList<CfgNode>(node.getPredecessors()); 
		for(CfgNode n : preds) {
			if(n.getId()==predId) {
				node.getPredecessors().remove(n);
			}
			
		} 
	}



}