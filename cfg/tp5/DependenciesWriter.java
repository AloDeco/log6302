package javaparser;

import java.util.*; 
import java.io.*; 

public class DependenciesWriter extends Writer {

	public DependenciesWriter(String fileName) {
		super(fileName);
	}
	
	public void writeGraphNodes(ArrayList<CfgNode> nodes) {	
		try {
			for (CfgNode node : nodes) {
				if(node.getName().contains("Begin") || node.getName().contains("End")) {continue;}
				file.write(node.getId() + " [ label = \" " + node.getId() + " - " + node.getName() + " \" ] \n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void writeGraphEdges(ArrayList<CfgNode[]> set) {	
		try {
			for (CfgNode[] edge : set) {
				if(edge[0].getName().contains("Begin") || edge[0].getName().contains("End")||edge[1].getName().contains("Begin") || edge[1].getName().contains("End")) {continue;}
				int fromnodeId = edge[0].getId();
				int tonodeId = edge[1].getId();					
				file.write(fromnodeId + "->" + tonodeId + " \n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}	

}