package javaparser;

import java.util.*; 
import java.io.*; 

public class Slicing {

	CfgGraph graph;
	ArrayList<CfgNode> nodes;
	ArrayList<CfgNode[]> pd;
	HashMap<String, ArrayList<CfgNode>> var;
	HashMap<String, ArrayList<CfgNode>> result = new HashMap<String, ArrayList<CfgNode>>();


	public Slicing(CfgGraph graph, ArrayList<CfgNode[]> cd, ArrayList<CfgNode[]> dd) {
		this.graph = graph;
		this.nodes = new ArrayList<CfgNode>(this.graph.getNodes());
		this.var = new HashMap<String, ArrayList<CfgNode>>(this.graph.getVar());
		this.pd = new ArrayList<CfgNode[]>(dd);
		this.pd.addAll(cd);
	}	


	public HashMap<String, ArrayList<CfgNode>> getSlicingForAllVar() {
		addPDPredecessorsToNodes();
		for(String v : var.keySet()) {
			for(CfgNode node : var.get(v)) {
				ArrayList<CfgNode> res = getSlicingForOneVarOneNode(node);
				orderNodes(res);
				result.put(v + ", " + node.getId() + " - " + node.getName(), res);
			}
		}
		return result;
	}

	private void addPDPredecessorsToNodes(){
		orderNodes(nodes);
		for(CfgNode[] edge : pd) {
			CfgNode node = nodes.get(edge[1].getId());
			CfgNode prednode = nodes.get(edge[0].getId());
			node.addPDPredecessor(prednode);
		}
	}

	public void orderNodes(ArrayList<CfgNode> nodeList) {
		Collections.sort(nodeList, new Comparator<CfgNode>() {
       		public int compare(CfgNode n1, CfgNode n2) {
            return new Integer(n1.getId()).compareTo(new Integer(n2.getId()));
        	}
    	});
	}


	private ArrayList<CfgNode> getSlicingForOneVarOneNode(CfgNode node) {
		ArrayList<CfgNode> res = new ArrayList<CfgNode>(node.getPDPredecessors());
		ArrayList<CfgNode> pdNodesFromLevelAbove = getPDNodesFromLevelAbove(res);
		while(pdNodesFromLevelAbove.size()!=0) {
			pdNodesFromLevelAbove = substraction(pdNodesFromLevelAbove, res);
			res.addAll(pdNodesFromLevelAbove);
			pdNodesFromLevelAbove = getPDNodesFromLevelAbove(pdNodesFromLevelAbove);
			//pdNodesFromLevelAbove.clear();
		}
		return res;
	}

	private ArrayList<CfgNode> getPDNodesFromLevelAbove(ArrayList<CfgNode> list) {
		ArrayList<CfgNode> res = new ArrayList<CfgNode>();
		for(CfgNode node : list) {
			res.addAll(node.getPDPredecessors());
		}
		return res;
	}


	public ArrayList<CfgNode> substraction(ArrayList<CfgNode> list1, ArrayList<CfgNode> list2) {
		if(list2==null) {
			return list1;
		} 
		ArrayList<CfgNode> list1copy = new ArrayList<CfgNode>(list1);
		list1copy.removeAll(list2);
		return list1copy;
	}

	
	
	private void print(String s) {
		System.out.println("--- " + s);
	}

}