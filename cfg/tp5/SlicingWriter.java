package javaparser;

import java.util.*; 
import java.io.*; 

public class SlicingWriter extends Writer {

	public SlicingWriter(String fileName) {
		super(fileName);
	}

	@Override
	public void writeGraphOpening() {
		try {
			file.write("digraph G {\n");
			file.write("node [\n\tfontname = \"Courier\"\n\tshape = \"record\"\n]\n");
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	public void writeGraphNodes(HashMap<String, ArrayList<CfgNode>> s) {	
		try {
			int i = 0;
			for(String varAndNode : s.keySet()) {
				if(s.get(varAndNode).size()==0) {continue;}
				file.write(i + " [ label = \"{" + "Critère \\n" + "(" + varAndNode + ")" +"|");
				for (CfgNode node : s.get(varAndNode)) {
					if(node.getName().contains("Begin") || node.getName().contains("End")) {continue;}
					file.write(node.getId() + " - " + node.getName() + "  \\l");
				}
				file.write("}\" ]\n");
				i++;
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

}