package javaparser;

import java.util.*; 
import java.io.*; 

public class DataDependencies {

	CfgGraph graph;
	ArrayList<CfgNode> nodes;
	HashMap<CfgNode, ArrayList<CfgNode>> rdefin;
	HashMap<CfgNode, String> defs;
	ArrayList<CfgNode> defandusenode;
	ArrayList<CfgNode[]> result = new ArrayList<CfgNode[]>();


	public DataDependencies(CfgGraph graph, HashMap<CfgNode, ArrayList<CfgNode>> rdefIn) {
		this.graph = graph;
		this.nodes = new ArrayList<CfgNode>(this.graph.getNodes());
		this.defs = new HashMap<CfgNode, String>(this.graph.getDef());
		this.rdefin = new HashMap<CfgNode, ArrayList<CfgNode>>(rdefIn);
		this.defandusenode = new ArrayList<CfgNode>(graph.getDefAndUseNodes());
	}	

	public ArrayList<CfgNode[]> computeDD() {
		for(CfgNode node : nodes) {
			ArrayList<CfgNode> inNodes = rdefin.get(node);
			for(CfgNode innode : inNodes) {
				if(defs.keySet().contains(innode)){ //if innode is a definition node
					String var = defs.get(innode);
					if(!innode.equals(node) && checkIfUse(node, var)) {
						addRelation(node, innode);
					} else if(innode.equals(node)) {  //if the node is a definition, check if it uses also the same variable it defines
						if(isDefAndUse(node)) {addRelation(node, node);}
					}
				}
			}
		}
		return result;
	}

	private boolean isDefAndUse(CfgNode node) {
		if(defandusenode.contains(node)) {
			return true;
		}
		return false;
	}

	private boolean checkIfUse(CfgNode node, String var) {
		SimpleNode astnode = node.getAstNode();
		if(astnode == null) {return false;}
		Token token = astnode.jjtGetFirstToken();
		boolean hasEqual = false;
		boolean hasVar = false;
		while(token !=null && token != astnode.jjtGetLastToken()) {
			if(astnode instanceof ForControl) {
				if(token.image.equals(var)) { return true; }
			} else {
				if(token.kind == 61) {
					hasEqual = true;
				} 
				if(token.image.equals(var)) {
					hasVar = true;
					if(hasEqual) { return true; }
				}
			}
			token = token.next;
		}

		//last iteration for last token
		if(astnode instanceof ForControl) {
			if(token.image.equals(var)) { return true; }
		} else {
			if(token.kind == 61) {
				hasEqual = true;
			} 
			if(token.image.equals(var)) {
				hasVar = true;
				if(hasEqual) { return true; }
			}
		}


		if(!hasEqual && hasVar) { return true; }
		return false;
	}

	private void addRelation(CfgNode node, CfgNode innode) {
		result.add(new CfgNode[]{innode, node});
	}
	
	private void print(String s) {
		System.out.println("--- " + s);
	}

}