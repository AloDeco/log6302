package javaparser;

import java.util.*; 
import java.io.*; 

public class ControlDependencies {

	CfgGraph graph;
	ArrayList<CfgNode> nodes; 
	HashMap<CfgNode, ArrayList<CfgNode>> pdom;
	HashMap<CfgNode, CfgNode> directpdom;
	ArrayList<CfgNode[]> setS = new ArrayList<CfgNode[]>();
	ArrayList<CfgNode[]> setCD = new ArrayList<CfgNode[]>();

	public ControlDependencies(CfgGraph graph, HashMap<CfgNode, ArrayList<CfgNode>> pdom, HashMap<CfgNode, CfgNode> directpdom) {
		this.graph = graph;
		this.nodes = this.graph.getNodes();
		this.pdom = new HashMap<CfgNode, ArrayList<CfgNode>>(pdom);
		this.directpdom = new HashMap<CfgNode, CfgNode>(directpdom);
	}	

	
	public ArrayList<CfgNode[]> computeCD() {
		computeS();
		for (CfgNode[] edge : setS) {
			CfgNode node = edge[1];
			do {
				setCD.add(new CfgNode[]{edge[0],node});
				node = ipdom(node);
			} while(!node.equals(ipdom(edge[0])));
		}
		return setCD;
	}

	private void computeS() {
		cleanpdom();
		for(CfgNode tonode : nodes) {
			for(CfgNode fromnode : tonode.getPredecessors()){
				if(!ypdomx(fromnode, tonode)){
					setS.add(new CfgNode[]{fromnode, tonode});
				} 
			}
		}
	}

	private void cleanpdom() {
		/* To remove a node from its postdominators list */
		ArrayList<CfgNode> keys = new ArrayList<>(pdom.keySet());
		for(CfgNode node : keys){
			ArrayList<CfgNode> pdomswithoutnode = new ArrayList<>(pdom.remove(node));
			pdomswithoutnode.remove(node);
			pdom.put(node, pdomswithoutnode);
		}
	}

	private boolean ypdomx(CfgNode x, CfgNode y) {
		if(pdom.get(x).contains(y)){
			return true;
		}
		return false;
	}

	private CfgNode ipdom(CfgNode node) {
		return directpdom.get(node);
	}

	


}