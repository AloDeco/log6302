package javaparser;

import java.util.*; 
import java.io.*; 

public class CfgEdge {

	private int idA;
	private int idB;

	public CfgEdge(int idA, int idB) {
		this.idA = idA;
		this.idB = idB;
	}

	public String getRelation() {
		return Integer.toString(idA) + "->" + Integer.toString(idB);
	}

	public int getPredId() {
		return this.idA;
	}
	public int getChildId() {
		return this.idB;
	}
}