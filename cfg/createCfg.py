import os
import timeit

start = timeit.default_timer()

noFile = True

for file in os.listdir("./cfg/dotfiles/"):
    if file.endswith(".dot"):
        noFile = False
        dotfile = "./cfg/dotfiles/" + file
        pdfgraph = "./cfg/graphs/" + file[:-4] + ".pdf"
        os.system("dot -T pdf -o "+ pdfgraph +" " + dotfile)

stop = timeit.default_timer()

duration = round(stop - start, 3)

if(noFile) :
    print("No graph to create")
else :
    print("Created all cfg graphs in " + str(duration) + " secondes")