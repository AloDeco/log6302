package javaparser;

import java.util.*; 
import java.io.*; 

public class DominatorsTree {

	CfgGraph graph;
	ArrayList<CfgNode> nodes; 
	HashMap<CfgNode, ArrayList<CfgNode>> dom = new HashMap<CfgNode, ArrayList<CfgNode>>();
	HashMap<CfgNode, CfgNode> directDoms = new HashMap<CfgNode, CfgNode>();
	ArrayList<CfgNode> nodesNoEntry;

	public DominatorsTree(CfgGraph graph) {
		this.graph = graph;
		this.nodes = this.graph.getNodes();
	}

	public HashMap<CfgNode, ArrayList<CfgNode>> getDom() {
		return this.dom;
	}

	public void init() {
		nodesNoEntry = new ArrayList<CfgNode>(nodes.subList(1,nodes.size()));

		ArrayList<CfgNode> entryDom = new ArrayList<CfgNode>();
		entryDom.add(nodes.get(0));
		dom.put(nodes.get(0), entryDom);  // dominator of the entry node is the entry itself
		
		for(CfgNode node : nodesNoEntry) {  // for all other nodes, set all nodes as the dominators
			dom.put(node, nodes);
		}
	}

	public void getDoms() {
		init();
		boolean change = true;
		//int iter = 0;
		while (change) {
			//iter++;
			change = false;
			for (CfgNode node : nodesNoEntry) {
				ArrayList<CfgNode> newdom = nodeUnionIntersectionPred(node); 
				if(!samelists(dom.get(node), newdom)) { //if the new list of dominators for this node is not the same as previously 
					dom.put(node, newdom);    //update the list of dominators
					change = true;
				}
			}
		}
		//System.out.println("Number of iterations for fixpoint dominators algorithm : " + iter);
	}

	public ArrayList<CfgNode> nodeUnionIntersectionPred(CfgNode node) {
		ArrayList<CfgNode> predecessors = node.getPredecessors();
		ArrayList<CfgNode> result = new ArrayList<CfgNode>(dom.get(predecessors.get(0)));  // the dom of the first predecessor
		for (int i = 1; i < predecessors.size(); i++) {   // for all the predecessors
			result = intersection(result, dom.get(predecessors.get(i)));
		}
		
		result = union(node, result);//union with the current node
		return result ;
	}

	public ArrayList<CfgNode> intersection(ArrayList<CfgNode> list1, ArrayList<CfgNode> list2) {
		list1.retainAll(list2);
		return list1;
	}

	public ArrayList<CfgNode> union(CfgNode node, ArrayList<CfgNode> list) {
		if(!list.contains(node)) {
			list.add(node);
		}
		return list;
	}

	public boolean samelists(ArrayList<CfgNode> oldlist, ArrayList<CfgNode> newlist) {
		/*Compare if 2 lists are equals, regardless the order of elements */
		Set<CfgNode> oldset = new HashSet<CfgNode>(oldlist);
		Set<CfgNode> newset = new HashSet<CfgNode>(newlist);
		return newset.equals(oldset);
	}



	public HashMap<CfgNode, CfgNode> getDirectDoms() {
		getDoms();
		for(CfgNode node : nodesNoEntry) {
			directDoms.put(node, getDirectDomOfNode(node));
		}
		return directDoms;
	}

	public CfgNode getDirectDomOfNode(CfgNode node) {
		CfgNode directdom = dom.get(node).get(0);
		for (CfgNode domnode : dom.get(node)) {
			if(directdom.equals(node) || !domnode.equals(node) && dom.get(domnode).contains(directdom)){
				directdom = domnode;
			}
		}
		return directdom;
	}

}