package javaparser;

import java.util.*; 
import java.io.*; 

public class DominatorsWriter extends Writer {

	DominatorsTree domTree; 

	public DominatorsWriter(String fileName) {
		super(fileName);
	}
	
	public void writeGraphNodes(ArrayList<CfgNode> nodes) {	
		try {
			for (CfgNode node : nodes) {
				file.write(node.getId() + " [ label = \" " + node.getName() + " \" ] \n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

	public void writeGraphEdges(HashMap<CfgNode, CfgNode> dom) {	
		Set<CfgNode> keys = dom.keySet();
		try {
			for (CfgNode node : keys) {
				int nodeId = node.getId();
				CfgNode dominator = dom.get(node);
				
				int domId = dominator.getId();
					
				file.write(domId + "->" + nodeId + " \n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}
	

}