package javaparser;

import java.util.*; 
import java.io.*; 

public class RdefWriter extends CfgWriter{

	public RdefWriter(String fileName) {
		super(fileName);
	}

	@Override
	public void writeGraphOpening() {
		try {
			file.write("digraph G {\n");
			file.write("node [\n\tfontname = \"Courier\"\n\tshape = \"record\"\n]\n");
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

	@Override
	public void writeGraphNodes(ArrayList<CfgNode> nodes) {	
		try {
			for (CfgNode node : nodes) {
				file.write(node.getId() + " [ label = \"{" + node.getId() + " - " + node.getName());
				writeGen(node);
				writeKill(node);
				writeIn(node);
				writeOut(node);
				
				file.write("}\" ]\n");
			}
			file.flush();
		} catch (IOException e) {
            e.printStackTrace();
        }
	}

	private void writeGen(CfgNode node) throws IOException {
		CfgNode gen = node.getGen();
		if(gen == null) {
			file.write("|GEN = \\{\\} \\l");
		} else {
			file.write("|GEN = \\{" + node.getGen().getId() + "\\} \\l");
		}
	}
	private void writeKill(CfgNode node) throws IOException {
		ArrayList<CfgNode> kill = node.getKill();
		if(kill == null) {
			file.write("KILL = \\{\\} \\l");
		} else {
			file.write("KILL = " + normalizeArrayToPrint(node.getKill()) + "\\l");
		}
	}

	private void writeIn(CfgNode node) throws IOException {
		file.write("IN = " + normalizeArrayToPrint(node.getIn()) + "\\l");
	}

	private void writeOut(CfgNode node) throws IOException {
		file.write("OUT = " + normalizeArrayToPrint(node.getOut()) + "\\l");
	}

	private String normalizeArrayToPrint(ArrayList<CfgNode> list) {
		return list.toString().replace("[", "\\{").replace("]", "\\}");
	}


}