package javaparser;

import java.util.*; 
import java.io.*; 

public class ReachingDef {

	CfgGraph graph;
	ArrayList<CfgNode> nodes; 
	HashMap<CfgNode, CfgNode> gen = new HashMap<CfgNode, CfgNode>();
	HashMap<CfgNode, ArrayList<CfgNode>> kill = new HashMap<CfgNode, ArrayList<CfgNode>>();
	HashMap<CfgNode, ArrayList<CfgNode>> in = new HashMap<CfgNode, ArrayList<CfgNode>>();
	HashMap<CfgNode, ArrayList<CfgNode>> out = new HashMap<CfgNode, ArrayList<CfgNode>>();
	HashMap<String, ArrayList<CfgNode>> var;

	public ReachingDef(CfgGraph graph) {
		this.graph = graph;
		this.nodes = this.graph.getNodes();
		this.var = new HashMap<String, ArrayList<CfgNode>>(this.graph.getVar());
	}	

	public void initGenKill() {
		ArrayList<CfgNode> nodeWithoutVar = new ArrayList<CfgNode>();
		for(String v : var.keySet()) {
			nodeWithoutVar.addAll(var.get(v));
		}
		for(CfgNode node : nodes) {
			if (!nodeWithoutVar.contains(node)) {
				putGen(node, null);
				putKill(node, null);
			} else {
				putGen(node, node);
			}
		}
		initNonNullKill();
	}
	public void initNonNullKill() {
		for(String v : var.keySet()) {
			ArrayList<CfgNode> varcopy = new ArrayList<CfgNode>(var.get(v));
			for(CfgNode node : varcopy) {
				putKill(node, new ArrayList(var.get(v)));
			}
		}
	}

	public void initInOut() {
		for(CfgNode node : nodes) {  
			in.put(node, new ArrayList<CfgNode>());
			out.put(node, new ArrayList<CfgNode>());
		}
	}

	public CfgGraph getOut() {
		initGenKill();
		initInOut();
		boolean change = true;
		//int iter = 0;
		while (change) {
			//iter++;
			change = false;
			for (CfgNode node : nodes) {
				in.put(node, computeIn(node));
				ArrayList<CfgNode> newout = computeOut(node); 
				if(!samelists(out.get(node), newout)) { //if the new out for this node is not the same as previously
					out.put(node, newout);    //update the out 
					change = true;
				}
			}
		}
		setNodesInOut();
		//System.out.println("Number of iterations for fixpoint reaching definitions algorithm : " + iter);

		return new CfgGraph(graph.getName(), nodes, graph.getEdges());
	}

	public ArrayList<CfgNode> computeIn(CfgNode node) {
		ArrayList<CfgNode> predecessors = new ArrayList(node.getPredecessors());
		if(predecessors.size() == 0) { //node Entry
			return new ArrayList<CfgNode>();
		}
		ArrayList<CfgNode> result = new ArrayList<CfgNode>(out.get(predecessors.get(0)));  // the out of the first predecessor
		for (int i = 1; i < predecessors.size(); i++) {   // for all the predecessors
			result = unionOfLists(result, out.get(predecessors.get(i)));
		}
		return result;
	}

	public ArrayList<CfgNode> computeOut(CfgNode node) {
		ArrayList<CfgNode> result = substraction(in.get(node), kill.get(node)); 
		result = unionOfNodeAndList(gen.get(node), result);
		return result;
	}

	public ArrayList<CfgNode> substraction(ArrayList<CfgNode> in, ArrayList<CfgNode> kill) {
		if(kill==null) {
			return in;
		} 
		ArrayList<CfgNode> incopy = new ArrayList<CfgNode>(in);
		incopy.removeAll(kill);
		return incopy;
	}

	public ArrayList<CfgNode> unionOfNodeAndList(CfgNode node, ArrayList<CfgNode> list) {
		if(node == null) {
			return list;
		}
		if(!list.contains(node)) {
			list.add(node);
		}
		return list;
	}

	public ArrayList<CfgNode> unionOfLists(ArrayList<CfgNode> list1, ArrayList<CfgNode> list2) {
		ArrayList<CfgNode> list1Copy = new ArrayList<CfgNode>(list1);
		ArrayList<CfgNode> list2Copy = new ArrayList<CfgNode>(list2);
		if(list1Copy.size()==0) {
			return list2Copy;
		}
		if(list2Copy.size()==0) {
			return list1Copy;
		}
        list2Copy.removeAll(list1Copy);
        list1Copy.addAll(list2Copy);
		return list1Copy;
	}

	public boolean samelists(ArrayList<CfgNode> oldlist, ArrayList<CfgNode> newlist) {
		/*Compare if 2 lists are equals, regardless the order of elements */
		Set<CfgNode> oldset = new HashSet<CfgNode>(oldlist);
		Set<CfgNode> newset = new HashSet<CfgNode>(newlist);
		return newset.equals(oldset);
	}

	
	public void putGen(CfgNode node, CfgNode gen) {
		this.gen.put(node, gen);
		node.setGen(gen);
	}

	public void putKill(CfgNode node, ArrayList<CfgNode> kill) {
		this.kill.put(node, kill);
		node.setKill(kill);
	}

	public void setNodesInOut() {
		for (CfgNode node : nodes) {
			node.setIn(in.get(node));
			node.setOut(out.get(node));
		} 
	}



	/*** Verification fonctions for tests ***/

	public void verifyResult() {
		for (CfgNode n : out.keySet()) {
			System.out.println(">>>>>>>>>>>>>>>>> Node : " + n.getName() + "  " + n.getId());
			for (CfgNode nc : out.get(n)) {
				System.out.println(nc.getName() + "  " + nc.getId());
			}
		}
	}

	public void verifyVar() {
		for (String v : var.keySet()) {
			System.out.println(">>>>>>>>>>>>>>>>> Var : " + v);
			for (CfgNode nc : var.get(v)) {
				System.out.println(nc.getName() + "  " + nc.getId());
			}
		}
	}

	public void verifyKill() {
		for (CfgNode n : kill.keySet()) {
			System.out.println(">>>>>>>>>>>>>>>>> Node : " + n.getName() + "  " + n.getId());
			if(kill.get(n)!=null) {
				for (CfgNode nc : kill.get(n)) {
					System.out.println(nc.getName() + "  " + nc.getId());
				}
			}
		}
	}
	public void verifyKillForNode() {
		for (CfgNode n : kill.keySet()) {
			System.out.println(">>>>>>>>>>>>>>>>> Node : " + n.getName() + "  " + n.getId());
			if(kill.get(n)!=null) {
				for (CfgNode nc : kill.get(n)) {
					System.out.println(nc.getName() + "  " + nc.getId());
				}
			}
		}
	}

	public void verifyGen() {
		for (CfgNode n : gen.keySet()) {
			System.out.println(">>>>>>>>>>>>>>>>> Node : " + n.getName() + "  " + n.getId());
			if(gen.get(n)!=null) {
				CfgNode nc = gen.get(n);
				System.out.println(nc.getName() + "  " + nc.getId());
			} else {
				System.out.println("null");
			}
		}
	}


}