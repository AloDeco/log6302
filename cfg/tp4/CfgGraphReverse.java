package javaparser;

import java.util.*; 
import java.io.*; 

public class CfgGraphReverse extends CfgGraph {

	CfgGraph graph;

	public CfgGraphReverse(String name, CfgGraph graph) {
		super(name);
		this.graph = graph;
	}

	public void setNodes() {
		createNodes();
		createEdges();
	}

	private void createNodes() {
		// add exit as first node
		CfgNode exit = graph.getNodes().get(graph.getNodes().size()-1);
		nodes.add(new CfgNode(exit.getId(), exit.getName()));

		for (CfgNode node : graph.getNodes().subList(0, graph.getNodes().size()-1)) {
			nodes.add(new CfgNode(node.getId(), node.getName()));
		}
	}
	private void createEdges() {
		for (CfgEdge edge : graph.getEdges()) {
			edges.add(new CfgEdge(edge.getChildId(), edge.getPredId()));
			addPredecessor(edge.getChildId(), edge.getPredId());
		}
	}
	
}